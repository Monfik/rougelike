using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length;
    private float startPosX;
    public float parralaxEffect;

    [SerializeField]
    private SpriteRenderer sprite;

    [SerializeField]
    new private Camera camera;

    void Start()
    {
        startPosX = transform.position.x;
        length = sprite.bounds.size.x;
    }

    private void FixedUpdate()
    {
        float dist = camera.transform.position.x * parralaxEffect;

        transform.position = new Vector3(startPosX + dist, transform.position.y, transform.position.z);
    }
}
