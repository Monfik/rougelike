using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BP
{
    public class Backpack : MonoBehaviour
    {
        public int maxWeight = 500;

        [SerializeField]
        private int currentWeight = 0;

        public int maxItemsCount = 20;
        private int currentItemsCount => items.Count;

        public List<InventorySlot> slots;
        public List<Item> items;

        [Header("References")]
        [SerializeField]
        private BackpackInput bpInput;
        public BackpackInput BpInput => bpInput;

        [SerializeField]
        private BackpackPanelMovement bpMovement;
        public BackpackPanelMovement BpMovement => bpMovement;

        //Test
        public Item itemToAdd;
        public List<Item> itemsToAdd;

        [ContextMenu("Add single item")]
        public void AddItem()
        {
            AddItem(itemToAdd);
        }

        [ContextMenu("Add multiple items")]
        public void AddMultipleItems()
        {
            AddStartItems();
        }

        private void Start()
        {
            AddStartItems();
        }

        private void AddStartItems()
        {
            foreach(Item item in itemsToAdd)
            {
                AddItem(item);
            }
        }

        public void AddItem(Item item)
        {
            if (currentItemsCount >= slots.Count)
            {
                ScreenConsole.LogError("U have no space in backpack!");
                return;
            }

            InventorySlot emptySlot = slots.Find(s => s.IsEmpty());
            if(emptySlot)
            {
                emptySlot.SetItem(item);
                items.Add(item);
                item.Reset();
            }
        }

        public bool CanAddItem()
        {
            return currentItemsCount < maxItemsCount;
        }

        public void DropItem(Item item, InventorySlot sourceSlot)
        {
            items.Remove(item);
            sourceSlot.ClearSlot();
            Drop(item);
        }

        public void Toggle()
        {
            bpMovement.Toogle();
        }

        public void ItemDropedOnSlot(InventorySlot sourceSlot, InventorySlot targetSlot)
        {
            if (sourceSlot == targetSlot)
                return;

            if (targetSlot.IsEmpty())
                ChangeSlot(sourceSlot, targetSlot);
            else
                SwapSlots(sourceSlot, targetSlot);
        }

        private void ChangeSlot(InventorySlot sourceSlot, InventorySlot targetSlot)
        {
            targetSlot.SetItem(sourceSlot.Item);
            sourceSlot.ClearSlot();
        }

        private void SwapSlots(InventorySlot sourceSlot, InventorySlot targetSlot)
        {
            Item tempItem = targetSlot.Item;
            targetSlot.SetItem(sourceSlot.Item);
            sourceSlot.SetItem(tempItem);
        }

        public void Drop(Item item)
        {
            //TODO
        }

        public void RemoveFromInventory(Item item)
        {
            items.Remove(item);
            item.owner.ClearSlot();
        }
    }
}
