using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Data;
using CustomInput;

namespace BP
{
    public class BackpackInput : MonoBehaviour
    {
        private InputMaster controls;

        [SerializeField]
        private Backpack bp;

        public void ReadInput(InputController inputController)
        {
            inputController.AddNewInput(Utils.Values.InputValues.B_KEY_BACKPACK_OPEN, KeyCode.B, ToogleBackpackGUI, null, null);
        }
        
        public void StopReadInput(InputController inputController)
        {
            inputController.RemoveInput(Utils.Values.InputValues.B_KEY_BACKPACK_OPEN);
        }

        private void ToogleBackpackGUI()
        {
            bp.Toggle();
        }
    }
}
