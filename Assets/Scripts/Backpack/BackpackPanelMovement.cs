using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BP
{
    public class BackpackPanelMovement : MonoBehaviour
    {
        private bool isAnimating;

        [SerializeField]
        private RectTransform rect;

        [SerializeField]
        private CanvasGroup canvasGroup;

        public float hidedCanvasTargetAlpha;
        public float showedCanvasTargetAlpha;

        public Vector2 hidePosition;
        public Vector2 showPosition;
        public float time;
        private float currentTime = 0f;

        private Vector2 startPosition;
        private Vector2 targetPosition;

        private float currentAlpha;
        private float targetAlpha;

        private bool opened;

        void Start()
        {
            isAnimating = false;
            opened = false;
            currentTime = 0f;
        }

        // Update is called once per frame
        void Update()
        {
            if (isAnimating)
            {
                currentTime += Time.deltaTime;
                float t = currentTime / time;

                Vector2 currentPosition = Mathfx.Hermite(startPosition, targetPosition, t);
                float alpha = Mathfx.Hermite(currentAlpha, targetAlpha, t);
             
                rect.anchoredPosition = currentPosition;
                canvasGroup.alpha = alpha;

                if (currentTime >= time)
                {
                    isAnimating = false;
                    currentTime = 0f;
                }
            }
        }

        public void Show()
        {
            startPosition = rect.anchoredPosition;
            targetPosition = showPosition;
            currentTime = 0f;
            targetAlpha = showedCanvasTargetAlpha;
            currentAlpha = canvasGroup.alpha;

            isAnimating = true;
        }

        public void Hide()
        {
            startPosition = rect.anchoredPosition;
            targetPosition = hidePosition;        
            currentTime = 0f;
            targetAlpha = hidedCanvasTargetAlpha;
            currentAlpha = canvasGroup.alpha;

            isAnimating = true;
        }

        public void Toogle()
        {
            if (opened)
            {
                Hide();
            }
            else
                Show();

            opened = !opened;         
        }
    }

}