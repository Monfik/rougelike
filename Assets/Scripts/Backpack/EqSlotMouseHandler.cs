using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BP;
using UnityEngine.EventSystems;


public class EqSlotMouseHandler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private EqSlot slot;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!eventData.dragging)
            slot.Clicked(eventData);
    }
}
