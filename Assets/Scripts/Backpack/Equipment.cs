using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BP
{
    public class Equipment : MonoBehaviour
    {
        public EqSlot helmet;
        public EqSlot armor;
        public EqSlot legs;
        public EqSlot boots;
        public EqSlot mainWeapon;
        public EqSlot secondaryWeapon;
        
        [Header("15")]
        [SerializeField]
        private Backpack bp;

        public void TryEquip(EqItem item)
        {         
            Equip(item);
        }

        public void Equip(EqItem item)
        {
            switch (item.type)
            {
                case EqItem.Type.Helmet:
                    Equip(item, helmet);
                    break;
                case EqItem.Type.Armor:
                    Equip(item, armor);
                    break;                
                case EqItem.Type.Legs:
                    Equip(item, legs);
                    break;
                case EqItem.Type.Boots:
                    Equip(item, boots);
                    break;
                case EqItem.Type.Main_Weapon:
                    Equip(item, mainWeapon);
                    break;
                case EqItem.Type.Secondary_Weapon:
                    Equip(item, secondaryWeapon);
                    break;
                default:
                    ScreenConsole.LogError("There is no item type matched to eq!");
                    break;
            }
        }

        private void Equip(EqItem item, EqSlot slot)
        {
            bp.RemoveFromInventory(item);

            if (!slot.IsEmpty())
            {
                TryDequipToBackpack(slot.eqItem);
            }

            slot.SetItem(item);
            item.equipped = true;

            ScreenConsole.Log("You have equipped " + item.ItemName);
            Core.GameManager.Instance.Storage.EventsStorage.PlayerEv.CallOnItemEquipped(item);
        }

        public void TryDequipToBackpack(EqItem item)
        {
            bool isBpFull = !bp.CanAddItem();

            if(isBpFull)
            {
                ScreenConsole.LogError("You have no space in backpack");
                return;
            }
            else
            {
                DequipToBackpack(item);
            }
        }

        public void DequipToBackpack(EqItem item)
        {
            Dequip(item);
            bp.AddItem(item);
        }
        
        private void Dequip(EqItem item)
        {
            item.owner.ClearSlot();
            item.equipped = false;
            Core.GameManager.Instance.Storage.EventsStorage.PlayerEv.CallOnItemDequipped(item);
        }

        public void DropItem(EqItem item, EqSlot itemOwner)
        {
            //TODO
            Dequip(item);
            bp.Drop(item);
        }
    }
}
