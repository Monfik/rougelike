using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Tools;

namespace BP
{
    public class InventorySlotMouseHandler : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private InventorySlot slot;

        [Header("Time")]
        [SerializeField]
        private float timeToDisplayItemInfo = 1f;

        private Timer highlightTimer;

        

        public void OnPointerClick(PointerEventData eventData)
        {
            if(!eventData.dragging)
                slot.Clicked(eventData);           
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            highlightTimer = new Timer(timeToDisplayItemInfo, HighlightTimeReached);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            highlightTimer = null;
        }

        private void Update()
        {
            if (highlightTimer != null)
            {
                highlightTimer.Update(Time.deltaTime);
            }
        }

        private void HighlightTimeReached()
        {
            Debug.Log("Display info about item");
            slot.DisplayItemInfo();
            highlightTimer = null;
        }
    }
}
