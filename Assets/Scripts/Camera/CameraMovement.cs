using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using Vocations;
using Core;

namespace CameraHandle
{
    public class CameraMovement : MonoBehaviour
    {
        private Transform beginMapPoint;
        private Transform endMapPoint;

        private Vocation playerVocation;

        [SerializeField]
        new private Camera camera;

        void Awake()
        {
            GameManager.Instance.Storage.EventsStorage.OnPlayerSpawned += OnPlayerSpawned;
            GameManager.Instance.Storage.EventsStorage.CoreEvnts.OnLevelLoaded += OnLevelLoaded;
        }

        void Update()
        {
            if (!playerVocation)
            {
                Debug.LogError("PlayerVocation is not assigned to CameraMovement's property");
                return;
            }

            UpdateCamera();
        }

        private void UpdateCamera()
        {
            SetCameraPosition(playerVocation.transform.position);
        }

        private void OnPlayerSpawned(Vocation player)
        {
            SetCameraPosition(player.transform.position);
            playerVocation = player;
        }

        private void SetCameraPosition(Vector3 position)
        {
            float cameraWidth = camera.orthographicSize * 2f * camera.aspect;
            if (position.x < beginMapPoint.position.x + cameraWidth / 2)
                position.x = beginMapPoint.position.x + cameraWidth / 2;

            else if (position.x > endMapPoint.position.x - cameraWidth / 2)
                position.x = endMapPoint.position.x - cameraWidth / 2;

            transform.position = new Vector3(position.x, transform.position.y, transform.position.z);
        }

        private void OnLevelLoaded(LevelData levelData)
        {
            beginMapPoint = levelData.MapBeginTransform;
            endMapPoint = levelData.MapEndTransform;
        }
    }

}