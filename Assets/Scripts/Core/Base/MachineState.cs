﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public abstract class MachineState<T> : MonoBehaviour
    {
        public IBaseState<T> currentState;

        public abstract void ChangeState(IBaseState<T> newState);
        public abstract void UpdateMachine();
    }
}
