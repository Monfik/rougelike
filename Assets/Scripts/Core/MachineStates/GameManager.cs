using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Data;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance = null;
        public static GameManager Instance => instance;

        [SerializeField]
        private DataStorage storage;
        public DataStorage Storage => storage;

        [SerializeField]
        private MainController controller;

        private void Awake()
        {
            if (instance == null)
                instance = this;

            controller.Init(storage);
        }

        private void Start()
        {
            //Storage.EventsStorage.CoreEvnts.CallOnFreestyleStateEnter();
            //controller.ChangeState(new GameflowState());
        }

        public void GameStart()
        {
            controller.GameStarted();
        }
    }
}
