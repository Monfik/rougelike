﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class MainController : MachineState<MainController>
    {
        [SerializeField]
        private DataStorage storage;
        public DataStorage Storage => storage;

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }

        private void Update()
        {
            UpdateMachine();
        }

        private void OnGameflowStateEnter()
        {
            ChangeState(new GameflowState());
        }

        private void OnPlayerSpawned(Vocations.Vocation player)
        {
            storage.ReferenceStorage.player = player;
            player.Init(storage);
            OnGameflowStateEnter();
        }

        #region BaseImplementation
        public override void ChangeState(IBaseState<MainController> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {
            currentState?.UpdateState(this);
        }
        #endregion

        public void GameStarted()
        {
            storage.ReferenceStorage.PlayersPanelDataManager.Init(storage);
            storage.EventsStorage.OnPlayerSpawned += OnPlayerSpawned;
        }
    }

}