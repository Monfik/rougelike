using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class DeathState : IBaseState<MainController>
    {
        public void Init(MainController controller)
        {
            controller.Storage.ReferenceStorage.DeadAnimation.Play("Dead");
            controller.Storage.ReferenceStorage.Backpack.BpInput.StopReadInput(controller.Storage.ReferenceStorage.Input);
        }

        public void FixedUpdateState()
        {
            
        }

        public void UpdateState(MainController controller)
        {
            
        }

        public void Deinit(MainController controller)
        {
            
        }
    }
}
