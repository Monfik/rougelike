using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class GameflowState : IBaseState<MainController>
    {
        private MainController controller;
        private DataStorage storage;
        public void Init(MainController controller)
        {
            this.controller = controller;
            storage = controller.Storage;

            storage.ReferenceStorage.player?.StartMove();
            storage.ReferenceStorage.Backpack.BpInput.ReadInput(controller.Storage.ReferenceStorage.Input);

            storage.EventsStorage.PlayerEv.OnPlayerDead += OnPlayerDied;
            Debug.Log("Init gameflow state");
        }

        public void UpdateState(MainController controller)
        {
            
        }

        public void FixedUpdateState()
        {
            
        }

        public void Deinit(MainController controller)
        {
            storage.ReferenceStorage.Backpack.BpInput.StopReadInput(storage.ReferenceStorage.Input);
        }

        private void OnPlayerDied()
        {
            controller.ChangeState(new DeathState());
        }
    }
}
