﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class MainGameState : MachineState<MainGameState>
    {
        private DataStorage storage;
        public DataStorage Storage => storage;

        public override void ChangeState(IBaseState<MainGameState> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {

        }

        #region StateImplementation
        public void Init(MainController controller)
        {
            storage = controller.Storage;

            //storage.ReferenceStorage.GameManager.CmdDealCards();
        }

        public void UpdateState(MainController controller)
        {

        }

        public void Deinit(MainController controller)
        {
            
        }
        #endregion
    }

}