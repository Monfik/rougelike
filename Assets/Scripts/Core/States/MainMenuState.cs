﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class MainMenuState : IBaseState<MainController>
    {
        private MainController controller;
        private DataStorage storage;

        #region StateImplementation
        public void Init(MainController controller)
        {
            this.controller = controller;
            this.storage = controller.Storage;

            Debug.Log("MainMenuState");
        }

        public void UpdateState(MainController controller)
        {

        }

        public void Deinit(MainController controller)
        {
            
        }
        #endregion

        public void OnChangeConnectState()
        {
            
        }

        public void FixedUpdateState()
        {
            
        }
    }
}