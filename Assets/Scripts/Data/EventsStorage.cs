﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vocations;
using BP;

namespace GameEvents
{
    public class EventsStorage
    {
        private CoreEvents coreEvents = new CoreEvents();
        public CoreEvents CoreEvnts => coreEvents;

        private PlayerEvents playerEvents = new PlayerEvents();
        public PlayerEvents PlayerEv => playerEvents;

        public UnityAction OnPlayerSpawnedBasic;
        public UnityAction<Vocation> OnPlayerSpawned;
        public void CallOnPlayerSpawned(Vocation vocation)
        {
            OnPlayerSpawned?.Invoke(vocation);
            OnPlayerSpawnedBasic?.Invoke();
        }


    }
}
