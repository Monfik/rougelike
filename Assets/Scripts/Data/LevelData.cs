using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace Data
{
    public class LevelData : MonoBehaviour
    {
        [SerializeField]
        private Transform playerSpawnPoint;
        public Transform PlayerSpawnPoint => playerSpawnPoint;

        [SerializeField]
        private Transform mapBeginTransform;
        public Transform MapBeginTransform => mapBeginTransform;

        [SerializeField]
        private Transform mapEndTransform;
        public Transform MapEndTransform => mapEndTransform;

        private void Start()
        {
            GameManager.Instance.Storage.EventsStorage.CoreEvnts.CallOnLevelLoaded(this);
        }
    }
}
