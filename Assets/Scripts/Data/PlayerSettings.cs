using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vocations;

namespace Settings
{
    public class PlayerSettings : MonoBehaviour
    {
        [SerializeField]
        private PlayerSelectedSettings selectedSettings;
        public PlayerSelectedSettings SelectedSettings => selectedSettings;

    }

}