﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vocations;
using Effects;
using Tools;
using BP;
using PlayerData;
using LightControllers;
using CustomInput;

namespace Data
{
    public class ReferenceStorage : MonoBehaviour
    {
        public Vocation player;

        [SerializeField]
        private DamageNotificationSpawner notificationSpawner;
        public DamageNotificationSpawner NotificationSpawner => notificationSpawner;

        [SerializeField]
        private Backpack backpack;
        public Backpack Backpack => backpack;

        [SerializeField]
        private Equipment eq;
        public Equipment Eq => eq;

        [SerializeField]
        private UtilTools tools;
        public UtilTools Tools => tools;

        [SerializeField]
        private PlayerDataPanelManager playersPanelDataManager;
        public PlayerDataPanelManager PlayersPanelDataManager => playersPanelDataManager;

        [SerializeField]
        private LightController lightController;
        public LightController LightControler => lightController;

        [SerializeField]
        private Animator deathAnimation;
        public Animator DeadAnimation => deathAnimation;

        [SerializeField]
        private InputController input;
        public InputController Input => input;      
    }
}
