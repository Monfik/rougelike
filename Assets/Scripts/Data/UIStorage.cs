﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.ContextMenu;

namespace Data
{
    public class UIStorage : MonoBehaviour
    {
        [SerializeField]
        private UIContextMenu contextMenu;
        public UIContextMenu ContextMenu_ => contextMenu;
    }
}
