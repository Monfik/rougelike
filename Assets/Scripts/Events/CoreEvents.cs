﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Data;

public class CoreEvents
{
    public UnityAction<LevelData> OnLevelLoaded;
    public void CallOnLevelLoaded(LevelData levelData)
    {
        OnLevelLoaded?.Invoke(levelData);
    }

    public UnityAction OnGameFlowStateEnter;
    public void CallOnGameflowStateEnter()
    {
        OnGameFlowStateEnter?.Invoke();
    }

    public UnityAction OnFreestyleStateExit;
    public void CallOnFreestyleStateExit()
    {
        OnFreestyleStateExit?.Invoke();
    }

    public UnityAction OnLevelTransitionStateEnter;
    public void CallOnLevelTransitionStateEnter()
    {
        OnLevelTransitionStateEnter?.Invoke();
    }

}
