using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using BP;

namespace GameEvents
{
    public class PlayerEvents
    {
        public UnityAction<int> OnPlayerTakenDamage;
        public void CallOnPlayerTakenDamage(int damage)
        {
            OnPlayerTakenDamage?.Invoke(damage);
        }

        public UnityAction<EqItem> OnItemEquipped;
        public void CallOnItemEquipped(EqItem item)
        {
            OnItemEquipped?.Invoke(item);
        }

        public UnityAction<EqItem> OnItemDequipped;
        public void CallOnItemDequipped(EqItem item)
        {
            OnItemDequipped?.Invoke(item);
        }

        public UnityAction<int> OnPlayersLevelChanged;
        public void CallOnPlayersLevelChanged(int newLevel)
        {
            OnPlayersLevelChanged?.Invoke(newLevel);
        }

        public UnityAction<int> OnPlayersMaxHpChanged;
        public void CallOnPlayersMaxHpChanged(int maxHp)
        {
            OnPlayersMaxHpChanged?.Invoke(maxHp);
        }

        public UnityAction<int> OnPlayersHpChanged;
        public void CallOnPlayersHpChanged(int hp)
        {
            OnPlayersHpChanged?.Invoke(hp);
        }

        public UnityAction<int> OnPlayersMaxManaChanged;
        public void CallOnPlayersMaxManaChanged(int maxMana)
        {
            OnPlayersMaxManaChanged?.Invoke(maxMana);
        }

        public UnityAction<int> OnPlayersManaChanged;
        public void CallOnPlayersManaChanged(int mana)
        {
            OnPlayersManaChanged?.Invoke(mana);
        }

        public UnityAction<int> OnPlayersMovementSpeedChanged;
        public void CallOnPlayersMovementSpeedChanged(int movSpeed)
        {
            OnPlayersMovementSpeedChanged?.Invoke(movSpeed);
        }

        public UnityAction<int> OnPlayersJumpForceChanged;
        public void CallOnPlayersJumpForceChanged(int jumpForce)
        {
            OnPlayersJumpForceChanged?.Invoke(jumpForce);
        }

        public UnityAction<int> OnPlayersPowerChanged;
        public void CallOnPlayersPowerChanged(int power)
        {
            OnPlayersPowerChanged?.Invoke(power);
        }

        public UnityAction<int> OnPlayersMagicPowerChanged;
        public void CallOnPlayersMagicPowerChanged(int magicPower)
        {
            OnPlayersMagicPowerChanged?.Invoke(magicPower);
        }

        public UnityAction<int> OnPlayersDeffLevelChanged;
        public void CallOnPlayersDeffLevelChanged(int deffLevel)
        {
            OnPlayersDeffLevelChanged?.Invoke(deffLevel);
        }

        public UnityAction OnPlayerDead;
        public void CallOnPlayerDead()
        {
            OnPlayerDead?.Invoke();
        }
    }
}
