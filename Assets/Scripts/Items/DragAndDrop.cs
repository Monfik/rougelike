using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BP;
using UnityEngine.UI;

public class DragAndDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField]
    private InventorySlot slot;

    [SerializeField]
    private GraphicRaycaster raycaster;

    private Vector2 startPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;

        
        //if (Physics.Raycast(ray, out hit))
        //{
        //    Debug.Log(hit.transform.gameObject.name);
        //}
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        RaycastForDropElements(eventData);

        transform.position = startPosition;
    }

    private void RaycastForDropElements(PointerEventData eventData)
    {
        List<RaycastResult> results = new List<RaycastResult>();
        raycaster.Raycast(eventData, results);

        foreach (RaycastResult result in results)
        {
            InventorySlot targetSlot = result.gameObject.GetComponent<InventorySlot>();

            if (targetSlot)
            {
                Core.GameManager.Instance.Storage.ReferenceStorage.Backpack.ItemDropedOnSlot(slot, targetSlot);
                return;
            }

            Equipment eq = result.gameObject.GetComponent<Equipment>();

            if (eq)
            {
                slot.Item.UsedOnEq();
                return;
            }
        }
    }
}
