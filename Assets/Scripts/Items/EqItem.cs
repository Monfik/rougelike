using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.ContextMenu;

namespace BP
{
    [CreateAssetMenu(fileName = "Data", menuName = "Items/EqItem")]
    public class EqItem : Item, IStatSource
    {
        public bool equipped = false;


        [Header("Stats")]
        [SerializeField]
        private Stats stats = new Stats();

        virtual public void TryEquip()
        {
            Core.GameManager.Instance.Storage.ReferenceStorage.Eq.TryEquip(this);
        }

        public override void Use()
        {
            if(equipped)
            {
                TryDequip();
            }
            else
            {
                TryEquip();
            }         
        }

        public void TryDequip()
        {
            Core.GameManager.Instance.Storage.ReferenceStorage.Eq.TryDequipToBackpack(this);
        }

        override public void ShowContextMenu(Vector3 position)
        {
            if(!equipped)
            {
                List<ContextMenuOptionData> options = new List<ContextMenuOptionData>();
                options.Add(new ContextMenuOptionData(Use, "Equip", true));
                options.Add(new ContextMenuOptionData(owner.DropItem, "Drop", true));

                Core.GameManager.Instance.Storage.UIStorage.ContextMenu_.Create(options, position);
            }
            else
            {
                List<ContextMenuOptionData> options = new List<ContextMenuOptionData>();
                options.Add(new ContextMenuOptionData(TryDequip, "Dequip", true));
                options.Add(new ContextMenuOptionData(owner.DropItem, "Drop", true));

                Core.GameManager.Instance.Storage.UIStorage.ContextMenu_.Create(options, position);
            }
        }

        public override void UsedOnEq()
        {
            TryEquip();
        }

        public override void Reset()
        {
            equipped = false;
        }

        override public void DisplayInfo(Vector3 position)
        {

        }

        public Stats GetStats()
        {
            return stats;
        }
    }
}
