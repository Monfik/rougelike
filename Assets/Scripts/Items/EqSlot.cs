using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BP
{
    public class EqSlot : InventorySlot
    {
        [SerializeField]
        public EqItem eqItem;

        virtual public void SetItem(EqItem item)
        {
            this.eqItem = item;
            eqItem.owner = this;
            itemArtwork.enabled = true;
            itemArtwork.sprite = item.Artwork;
        }

        override public void ClearSlot()
        {
            eqItem = null;
            itemArtwork.sprite = null;
            itemArtwork.enabled = false;
            slotButton.onClick.RemoveAllListeners();
        }

        override public void Clicked(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                eqItem.ShowContextMenu(Rect.position);
            }
            else if (eventData.button == PointerEventData.InputButton.Left)
            {
                eqItem.Use();
            }
        }

        override public void DisplayItemInfo()
        {
            eqItem.DisplayInfo(Rect.position);
        }

        public override void DropItem()
        {
            Core.GameManager.Instance.Storage.ReferenceStorage.Eq.DropItem(eqItem, this);
        }
        public override bool IsEmpty()
        {
            return eqItem == null;
        }
    }
}
