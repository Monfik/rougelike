using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BP
{
    public class InventorySlot : MonoBehaviour
    {
        [SerializeField]
        protected Image itemArtwork;

        [SerializeField]
        private Item item;
        public Item Item => item;

        [SerializeField]
        protected Button slotButton;

        [SerializeField]
        private RectTransform rect;
        public RectTransform Rect => rect;

        [SerializeField]
        private InventorySlotMouseHandler mouseHandler;

        private void Start()
        {
            
        }

        public void SetItem(Item item)
        {
            this.item = item;
            item.owner = this;
            itemArtwork.enabled = true;
            itemArtwork.sprite = item.Artwork;
            mouseHandler.enabled = true;
        }


        virtual public void ClearSlot()
        {
            itemArtwork.sprite = null;
            itemArtwork.enabled = false;
            item = null;
            mouseHandler.enabled = false;
            slotButton.onClick.RemoveAllListeners();
        }

        virtual public void DropItem()
        {
            Core.GameManager.Instance.Storage.ReferenceStorage.Backpack.DropItem(item, this);
        }

        virtual public void Clicked(PointerEventData eventData)
        {
            if(eventData.button == PointerEventData.InputButton.Right)
            {
                item.ShowContextMenu(Rect.position);
            }
            else if(eventData.button == PointerEventData.InputButton.Left)
            {
                item.Use();
            }
        }

        virtual public bool IsEmpty()
        {
            return item == null;
        }

        virtual public void DisplayItemInfo()
        {
            item.DisplayInfo(Rect.position);
        }
    }

}