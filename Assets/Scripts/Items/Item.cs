using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.ContextMenu;

namespace BP
{
    [CreateAssetMenu(fileName = "Data", menuName = "Items/DefaultItem")]
    public class Item : ScriptableObject
    {
        public enum Type { Boots, Legs, Armor, Helmet, Main_Weapon, Secondary_Weapon, Tool, Eatable, Usable, Sellable };

        [SerializeField]
        protected string itemName;
        public string ItemName => itemName;

        [SerializeField]
        protected string description;

        [SerializeField]
        protected float weight;

        [SerializeField]
        protected int worth;

        [SerializeField]
        protected Sprite artwork;
        public Sprite Artwork => artwork;

        public Type type;

        [HideInInspector]
        public InventorySlot owner;

        virtual public void ShowContextMenu(Vector3 position)
        {

        }
        virtual public void Use()
        {

        }

        virtual public void UsedOnEq()
        {

        }
        virtual public void Reset()
        {

        }

        virtual public void DisplayInfo(Vector3 position)
        {
            
        }
    }

}