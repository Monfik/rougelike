using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using UnityEngine.Experimental.Rendering.Universal;

namespace LightControllers
{
    public class LightController : MonoBehaviour
    {
        // Start is called before the first frame update
        private DataStorage storage;

        [SerializeField]
        private Light2D globalLight;

        [SerializeField]
        private Animator globalLightAnimator;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }

        public void GlobalDarken()
        {
            globalLightAnimator.Play("DarkToBrightLight");
        }

        public void GlobalBright()
        {
            globalLightAnimator.Play("BrightToDarkLight");
        }
    }
}
