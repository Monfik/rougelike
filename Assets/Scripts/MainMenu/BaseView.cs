using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaseView : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup canvasGroup;
    public CanvasGroup Canvas => canvasGroup;

    [SerializeField]
    private RectTransform rectTransform;
    public RectTransform Rect => rectTransform;
}
