using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal;
using Vocations;

namespace Buttons
{
    public class VocationButton : MonoBehaviour
    {
        [SerializeField]
        private int assignedVocationIndex;
        public int AssignedVocationIndex => assignedVocationIndex;

        [SerializeField]
        private Button button;

        [SerializeField]
        private ParticleSystem glow;

        private void Start()
        {
            glow.Play();
        }

        public void Select()
        {      
            glow.gameObject.SetActive(true);
            glow.Play();
        }

        public void Deselect()
        {
            glow.gameObject.SetActive(false);
            glow.Stop();
        }
    }

}