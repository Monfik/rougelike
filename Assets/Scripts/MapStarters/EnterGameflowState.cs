using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterGameflowState : MonoBehaviour
{
    private void Awake()
    {
        //EnterGameflowStateNow();
    }

    private void OnPlayerSpawned()
    {

    }

    private void EnterGameflowStateNow()
    {
        Core.GameManager.Instance.Storage.EventsStorage.CoreEvnts.CallOnGameflowStateEnter();
    }
}
