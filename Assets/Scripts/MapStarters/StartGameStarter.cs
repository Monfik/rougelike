using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameStarter : MonoBehaviour
{
    private void Awake()
    {
        Core.GameManager.Instance.GameStart();
    }
}
