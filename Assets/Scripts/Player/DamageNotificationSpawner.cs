using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Effects
{
    public class DamageNotificationSpawner : MonoBehaviour
    {
        [SerializeField]
        private DamageNotification notification;

        public void SpawnNotification(string notification, Color color, Transform playerCanvasTransform)
        {
            this.notification.NotificationTMP.text = notification;
            this.notification.NotificationTMP.color = color;

            Instantiate(this.notification, playerCanvasTransform);
        }
    }
}
