using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
   
    private float horizontalMove = 0f;
    private bool jumped = false;
    private bool run = false;

    [SerializeField]
    private float runSpeed = 90f;
    [SerializeField]
    private float walkSpeed = 60f;

    [SerializeField]
    private CharacterController2D controller;

    [SerializeField]
    private Vocations.Vocation player;

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetButtonDown("Jump"))
    //    {
    //        jumped = true;
    //    }

    //    bool run = Input.GetButton("ExtraSpeed");

    //    if(run)
    //    {
    //        horizontalMove = Input.GetAxisRaw("Horizontal") * player.RunMovementSpeed;
    //    }
    //    else
    //    {
    //        horizontalMove = Input.GetAxisRaw("Horizontal") * player.WalkMovementSpeed;
    //    }
    //}

    //private void FixedUpdate()
    //{
    //    controller.Move(horizontalMove * Time.fixedDeltaTime, false, jumped);
    //    jumped = false;
    //}
}
