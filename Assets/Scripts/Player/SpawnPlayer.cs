using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vocations;
using Data;
using Settings;
using Core;
public class SpawnPlayer : MonoBehaviour
{
    public Vocation[] availableVocations;
    public DataStorage storage;

    [Header("Spawn in scene")]
    public bool spawnWithoutMenu;

    private PlayerSelectedSettings currentSettings;

    [HideInInspector]
    public PlayerSelectedSettings settings;


    [ContextMenu("spawn")]
    private void Awake()
    {
        GameManager.Instance.Storage.EventsStorage.CoreEvnts.OnLevelLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(LevelData levelData)
    {
        if (spawnWithoutMenu)
        {
            currentSettings = settings;
        }
        else
        {
            currentSettings = GameManager.Instance.Storage.PlayerSettings.SelectedSettings;
        }

        int vocIndex = currentSettings.selectedVocationIndex;
        Spawn(availableVocations[vocIndex - 1], levelData.PlayerSpawnPoint);
    }

    private void Spawn(Vocation playerVoc, Transform spawnPoint)
    {
        Vocation spawnedVocation = Instantiate(playerVoc);
        spawnedVocation.transform.position = spawnPoint.position;
        GameManager.Instance.Storage.EventsStorage.CallOnPlayerSpawned(spawnedVocation);
    }

    private void Respawn(Vocation playerVoc, Transform spawnPoint)
    {
        playerVoc.transform.position = spawnPoint.position;
    }
}
