using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using Settings;

[CustomEditor(typeof(SpawnPlayer))]
public class SpawnPlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // for other non-HideInInspector fields

        SpawnPlayer script = (SpawnPlayer)target;

        if(script.spawnWithoutMenu)
        {
            script.settings = EditorGUILayout.ObjectField("Settings object", script.settings, typeof(PlayerSelectedSettings), true) as PlayerSelectedSettings;
        }
    }
}
