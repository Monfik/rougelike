using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Tools;

namespace PlayerControllers
{
    public class PlayerAttackState : IBaseState<PlayerMachineState>
    {
        private PlayerMachineState controller;
        private Timer timer;
        public void Init(PlayerMachineState controller)
        {
            this.controller = controller;

            controller.Voc.Animatr.Play(Animator.StringToHash("Attack"));
            AnimatorClipInfo[] clipInfos = controller.Voc.Animatr.GetCurrentAnimatorClipInfo(0);
            timer = new Timer(clipInfos[0].clip.length, ChangeStateToIdle);
        }

        public void UpdateState(PlayerMachineState controller)
        {
            timer?.Update(Time.deltaTime);
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(PlayerMachineState controller)
        {
            
        }

        public void ChangeStateToIdle()
        {
            timer = null;
            controller.ChangeState(new PlayerMovementState());
        }
    }

}