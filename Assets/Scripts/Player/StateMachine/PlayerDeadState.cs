using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace PlayerControllers
{
    public class PlayerDeadState : IBaseState<PlayerMachineState>
    {
        public void Init(PlayerMachineState controller)
        {
            controller.Voc.Animatr.Play("Die");
        }

        public void UpdateState(PlayerMachineState controller)
        {
            
        }

        public void FixedUpdateState()
        {
            
        }
       
        public void Deinit(PlayerMachineState controller)
        {
            
        }
    }
    
}
