using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using UnityEngine.InputSystem;

namespace PlayerControllers
{
    public class PlayerJumpState : IBaseState<PlayerMachineState>
    {
        private float direction = 0f;
        private bool running = false;

        private PlayerMachineState controller;

        private InputAction movementAction;
        private InputAction runningAction;
        private InputMaster controls;
        
        public void Init(PlayerMachineState controller)
        {
            this.controller = controller;
            controller.Voc.Animatr.Play("Jump");

            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.A_KEY_MOVE_LEFT, KeyCode.A, null, OnLeft, null);
            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.D_KEY_MOVE_RIGHT, KeyCode.D, null, OnRight, null);
        }

        public void UpdateState(PlayerMachineState controller)
        {
            
        }

        public void FixedUpdateState()
        {
            controller.Voc.Move(direction * Time.fixedDeltaTime, false);
        }

        public void Deinit(PlayerMachineState controller)
        {
            
        }

        private void OnLeft()
        {
            direction = -1f;
        }

        private void OnRight()
        {
            direction = 1f;
        }
    }

}