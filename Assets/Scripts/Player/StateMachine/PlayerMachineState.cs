using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Vocations;

namespace PlayerControllers
{
    public class PlayerMachineState : MachineState<PlayerMachineState>
    {
        private Vocation vocation;
        public Vocation Voc => vocation;

        public void Init(Vocation vocation)
        {
            this.vocation = vocation;
        }

        public override void ChangeState(IBaseState<PlayerMachineState> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {
            currentState?.UpdateState(this);
        }

        private void Update()
        {
            currentState?.UpdateState(this);
        }

        private void FixedUpdate()
        {
            currentState?.FixedUpdateState();
        }
    }
}
