using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using UnityEngine.InputSystem;
using UnityEngine.Events;

namespace PlayerControllers
{
    public class PlayerMovementState : IBaseState<PlayerMachineState>
    {
        private float direction = 0f;
        private bool running = false;

        private PlayerMachineState controller;

        private UnityAction OnAttack;

        public void Init(PlayerMachineState controller)
        {
            //Debug.Log("PlayerIdleState");
            this.controller = controller;

            OnAttack += Attack;

            controller.Voc.Animatr.Play("Idle");
            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.A_KEY_MOVE_LEFT, KeyCode.A, null, OnLeft, null);
            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.D_KEY_MOVE_RIGHT, KeyCode.D, null, OnRight, null);
            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.SPACE_KEY_JUMP, KeyCode.Space, null, Jump, null);
            controller.Voc.Storage.ReferenceStorage.Input.AddNewInput(Utils.Values.InputValues.LEFTARROW_KEY_MOVE_LEFT, KeyCode.LeftShift, null, Run, null);
        }

        public void UpdateState(PlayerMachineState controller)
        {
            //controller.Voc.Animatr.SetFloat("Speed", Mathf.Abs(controller.Voc.Rigidbody.velocity.x));
            float speed = Mathf.Abs(controller.Voc.Rigidbody.velocity.x);

            if (speed == 0f)
                controller.Voc.Animatr.Play("Idle");
            else if (speed > controller.Voc.Stats_.runMovementSpeed)
                controller.Voc.Animatr.Play("Run");
            else
                controller.Voc.Animatr.Play("Walk");
        }

        public void FixedUpdateState()
        {
            controller.Voc.Move(direction * Time.fixedDeltaTime, running);
            running = false;
            direction = 0f;
        }

        public void Deinit(PlayerMachineState controller)
        {
            OnAttack -= Attack;

            controller.Voc.Storage.ReferenceStorage.Input.RemoveInput(Utils.Values.InputValues.A_KEY_MOVE_LEFT);
            controller.Voc.Storage.ReferenceStorage.Input.RemoveInput(Utils.Values.InputValues.D_KEY_MOVE_RIGHT);
            controller.Voc.Storage.ReferenceStorage.Input.RemoveInput(Utils.Values.InputValues.SPACE_KEY_JUMP);
            controller.Voc.Storage.ReferenceStorage.Input.RemoveInput(Utils.Values.InputValues.LEFTARROW_KEY_MOVE_LEFT);
        }

        private void Jump()
        {
            bool canJump = controller.Voc.Controller2D.canJump();

            if (canJump)
                controller.Voc.Jump();
        }

        private void Attack()
        {
            controller.Voc.Attack();
        }

        private void CallOnAttack()
        {
            OnAttack?.Invoke();
        }

        private void OnLeft()
        {
            direction = -1f;
        }

        private void OnRight()
        {
            direction = 1f;
        }

        private void Run()
        {
            running = true;
        }
    }
}
