using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace PlayerControllers
{
    public class PlayerNoneState : IBaseState<PlayerMachineState>
    {
        public void Init(PlayerMachineState controller)
        {

        }

        public void UpdateState(PlayerMachineState controller)
        {
            
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(PlayerMachineState controller)
        {

        }
    }
}
