using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace PlayerControllers
{
    public class PlayerWalkState : IBaseState<PlayerMachineState>
    {
        private PlayerMachineState controller;
        public void Init(PlayerMachineState controller)
        {
            this.controller = controller;
            controller.Voc.Animatr.Play("Walk");
            Debug.Log("Waaaalk");
        }

        public void UpdateState(PlayerMachineState controller)
        {
            
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(PlayerMachineState controller)
        {

        }
    }

}