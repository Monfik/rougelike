using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IStatSource
{
    Stats GetStats();
}
