using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

[System.Serializable]
public class Stats
{
    [Header("Properties")]
    public int maxHp;
    public int hp;
    public int maxMana;
    public int mana;
    public int walkMovementSpeed;
    public int runMovementSpeed;
    public int deffLevel;
    public int power;
    public int magicPower;
    public int jumpForce;

    public Stats Add(Stats stats)
    {
        Stats newStats = new Stats();

        newStats.maxHp = stats.maxHp + maxHp;
        newStats.hp = hp;   
        Mathf.Clamp(newStats.hp, 0, newStats.maxHp);
        newStats.maxMana = stats.maxMana + maxMana;
        newStats.mana = mana;
        Mathf.Clamp(newStats.mana, 0, newStats.maxMana);
        newStats.power = stats.power + power;
        newStats.magicPower = stats.magicPower + magicPower;
        newStats.deffLevel = stats.deffLevel + deffLevel;
        newStats.walkMovementSpeed = stats.walkMovementSpeed + walkMovementSpeed;
        newStats.runMovementSpeed = stats.runMovementSpeed + runMovementSpeed;
        newStats.jumpForce = stats.jumpForce + jumpForce;

        return newStats;
    }

    public Stats Remove(Stats stats)
    {
        Stats newStats = new Stats();

        newStats.maxHp = -stats.maxHp + maxHp;
        newStats.hp = hp;
        Mathf.Clamp(newStats.hp, 0, newStats.maxHp);
        newStats.maxMana = -stats.maxMana + maxMana;
        newStats.mana = mana;
        Mathf.Clamp(newStats.mana, 0, newStats.maxMana);
        newStats.power = -stats.power + power;
        newStats.magicPower = -stats.magicPower + magicPower;
        newStats.deffLevel = -stats.deffLevel + deffLevel;
        newStats.walkMovementSpeed = -stats.walkMovementSpeed + walkMovementSpeed;
        newStats.runMovementSpeed = -stats.runMovementSpeed + runMovementSpeed;
        newStats.jumpForce = -stats.jumpForce + jumpForce;

        return newStats;
    }

    public static void CompareAndCallEvents(Stats stats1, Stats stats2, PlayerEvents events)
    {
        if (stats1.maxHp != stats2.maxHp)
            events.CallOnPlayersMaxHpChanged(stats1.maxHp);
        if (stats1.hp != stats2.hp)
            events.CallOnPlayersHpChanged(stats1.hp);
        if (stats1.maxMana != stats2.maxMana)
            events.CallOnPlayersMaxManaChanged(stats1.maxMana);
        if (stats1.mana != stats2.mana)
            events.CallOnPlayersManaChanged(stats1.mana);
        if (stats1.power != stats2.power)
            events.CallOnPlayersPowerChanged(stats1.power);
        if (stats1.magicPower != stats2.magicPower)
            events.CallOnPlayersMagicPowerChanged(stats1.magicPower);
        if (stats1.deffLevel != stats2.deffLevel)
            events.CallOnPlayersDeffLevelChanged(stats1.deffLevel);
        if (stats1.walkMovementSpeed != stats2.walkMovementSpeed)
            events.CallOnPlayersMovementSpeedChanged(stats1.walkMovementSpeed);
        if (stats1.jumpForce != stats2.jumpForce)
            events.CallOnPlayersJumpForceChanged(stats1.jumpForce);
    }
}
