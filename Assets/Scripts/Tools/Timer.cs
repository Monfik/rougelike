using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Tools
{
    public class Timer
    {
        private float time;
        private float currentTime = 0f;
        private UnityAction funcToExecute;
        public Timer(float time, UnityAction func)
        {
            this.time = time;
            funcToExecute = func;
        }

        public void Update(float deltaTime)
        {
            currentTime += deltaTime;

            if (currentTime > time)
                funcToExecute?.Invoke();
        }
    }
}
