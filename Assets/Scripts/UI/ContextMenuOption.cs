using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace UI.ContextMenu
{
    public class ContextMenuOption : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private TMP_Text text;

        public void SetCallback(UnityAction callback, UnityAction closeMenuCallBack)
        {
            button.onClick.AddListener(callback);
            button.onClick.AddListener(closeMenuCallBack);
        }

        public void SetText(string optionText)
        {
            text.text = optionText;
        }

        public void Clear()
        {
            button.onClick.RemoveAllListeners();
            text.text = "";
        }

        public void SetOptionEnabled(bool enabled)
        {
            button.interactable = enabled;
        }
    }
}
