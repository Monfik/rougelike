using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI.ContextMenu
{
    public class ContextMenuOptionData
    {
        public ContextMenuOptionData(UnityAction callback, string optionText, bool enabled)
        {
            this.callback = callback;
            this.optionText = optionText;
            this.enabled = enabled;
        }

        public UnityAction callback;
        public string optionText;
        public bool enabled;
    }
}
