using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenConsole : MonoBehaviour
{
    private static ScreenConsole instance;

    [SerializeField]
    private float logTimeExist = 2f;

    [SerializeField]
    private TMP_Text logPrefab;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public static void LogError(string message)
    {
        instance.SpawnLog(message, Color.red);
    }

    public static void Log(string message)
    {
        instance.SpawnLog(message, Color.white);
    }

    private void SpawnLog(string message, Color color)
    {
        logPrefab.text = message;
        logPrefab.color = color;
        TMP_Text spawnedLog = Instantiate(logPrefab, transform);
        Destroy(spawnedLog, logTimeExist);
    }
}
