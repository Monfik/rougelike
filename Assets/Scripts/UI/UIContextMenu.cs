using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.ContextMenu
{
    public class UIContextMenu : MonoBehaviour
    {
        [SerializeField]
        private List<ContextMenuOption> options;

        [SerializeField]
        private RectTransform rect;
        public void Create(List<ContextMenuOptionData> optionsData, Vector2 position)
        {
            Close();

            if (options.Count < optionsData.Count)
            {
                Debug.LogError("Amount of options in context menu is lower than defined options !");
                return;
            }

            for (int i = 0; i < optionsData.Count; i++)
            {
                options[i].gameObject.SetActive(true);
                options[i].SetCallback(optionsData[i].callback, Close);
                options[i].SetText(optionsData[i].optionText);
                options[i].SetOptionEnabled(optionsData[i].enabled);
            }

            rect.anchoredPosition = position;
        }

        public void Close()
        {
            foreach (ContextMenuOption option in options)
            {
                option.gameObject.SetActive(false);
                option.Clear();
            }
        }
    }

}