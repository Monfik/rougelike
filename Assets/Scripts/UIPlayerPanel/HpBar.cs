using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vocations;
using Core;
public class HpBar : MonoBehaviour
{
    [SerializeField]
    private Slider slider;

    private int maxHp;
    private int currentHp;

    private void Awake()
    {
        GameManager.Instance.Storage.EventsStorage.OnPlayerSpawned += OnPlayerSpawned;
        GameManager.Instance.Storage.EventsStorage.PlayerEv.OnPlayersHpChanged += OnPlayerCurrentHpChanged;
        GameManager.Instance.Storage.EventsStorage.PlayerEv.OnPlayersMaxHpChanged += OnPlayerMaxHpChanged;

        slider.value = 1f;
    }
    private void UpdateBar(float hpFactor)
    {
        hpFactor = Mathf.Clamp(hpFactor, 0, 1);
        slider.value = hpFactor;
    }

    void OnPlayerSpawned(Vocation voc)
    {
        maxHp = voc.Stats_.maxHp;
        currentHp = voc.Stats_.hp;
        float t = (float)currentHp / (float)maxHp;
        UpdateBar(t);
    }

    private void OnPlayerMaxHpChanged(int maxHp)
    {
        this.maxHp = maxHp;
        float t = (float)currentHp / (float)maxHp;
        UpdateBar(t);
    }

    private void OnPlayerCurrentHpChanged(int currentHp)
    {
        this.currentHp = currentHp;
        float t = (float)currentHp / (float)maxHp;
        UpdateBar(t);
    }
}
