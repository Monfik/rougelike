using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vocations;
using Core;
public class ManaBar : MonoBehaviour
{
    [SerializeField]
    private Slider slider;

    private int maxMana;
    private int currentMana;

    private void Awake()
    {
        GameManager.Instance.Storage.EventsStorage.OnPlayerSpawned += OnPlayerSpawned;
        GameManager.Instance.Storage.EventsStorage.PlayerEv.OnPlayersManaChanged += OnPlayerCurrentManaChanged;
        GameManager.Instance.Storage.EventsStorage.PlayerEv.OnPlayersMaxManaChanged += OnPlayerMaxManaChanged;

        //slider.value = 1f;
    }
    private void UpdateBar(float manaFactor)
    {
        manaFactor = Mathf.Clamp(manaFactor, 0, 1);
        slider.value = manaFactor;
    }

    void OnPlayerSpawned(Vocation voc)
    {
        maxMana = voc.Stats_.maxMana;
        currentMana = voc.Stats_.mana;
        float t = (float)currentMana / (float)maxMana;
        UpdateBar(t);
    }

    private void OnPlayerMaxManaChanged(int maxMana)
    {
        this.maxMana = maxMana;
        float t = (float)currentMana / (float)maxMana;
        UpdateBar(t);
    }

    private void OnPlayerCurrentManaChanged(int currentMana)
    {
        this.currentMana = currentMana;
        float t = (float)currentMana / (float)maxMana;
        UpdateBar(t);
    }
}
