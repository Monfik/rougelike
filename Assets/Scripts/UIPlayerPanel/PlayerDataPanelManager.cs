using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using TMPro;

namespace PlayerData
{
    public class PlayerDataPanelManager : MonoBehaviour
    {
        private DataStorage storage;

        private int currentMaxHP;
        private int currentHP;
        private int currentMaxMana;
        private int currentMana;

        [SerializeField]
        private TMP_Text levelText;
        [SerializeField]
        private TMP_Text hpText;
        [SerializeField]
        private TMP_Text manaText;
        [SerializeField]
        private TMP_Text powerText;
        [SerializeField]
        private TMP_Text magicPowerText;
        [SerializeField]
        private TMP_Text deffendingText;
        [SerializeField]
        private TMP_Text movementSpeedText;
        [SerializeField]
        private TMP_Text jumpForceText;

        public void Init(DataStorage storage)
        {
            this.storage = storage;

            storage.EventsStorage.PlayerEv.OnPlayersLevelChanged += OnPlayersLevelChanged;
            storage.EventsStorage.PlayerEv.OnPlayersMaxHpChanged += OnPlayersMaxHpChanged;
            storage.EventsStorage.PlayerEv.OnPlayersHpChanged += OnPlayersHpChanged;
            storage.EventsStorage.PlayerEv.OnPlayersMaxManaChanged += OnPlayersMaxManaChanged;
            storage.EventsStorage.PlayerEv.OnPlayersManaChanged += OnPlayersManaChanged;
            storage.EventsStorage.PlayerEv.OnPlayersMovementSpeedChanged += OnPlayersMovementSpeedChanged;
            storage.EventsStorage.PlayerEv.OnPlayersJumpForceChanged += OnPlayersJumpForceChanged;
            storage.EventsStorage.PlayerEv.OnPlayersPowerChanged += OnPlayersPowerChanged;
            storage.EventsStorage.PlayerEv.OnPlayersMagicPowerChanged += OnPlayersMagicPowerChanged;
            storage.EventsStorage.PlayerEv.OnPlayersDeffLevelChanged += OnPlayersDeffLevelChanged;
            storage.EventsStorage.OnPlayerSpawned += OnPlayerSpawned;
        }

        private void OnPlayerSpawned(Vocations.Vocation player)
        {
            Stats playerStats = player.Stats_;
            OnPlayersLevelChanged(player.Level);
            OnPlayersMagicPowerChanged(playerStats.magicPower);
            OnPlayersMovementSpeedChanged(playerStats.walkMovementSpeed);
            OnPlayersJumpForceChanged(playerStats.jumpForce);
            OnPlayersPowerChanged(playerStats.power);
            OnPlayersDeffLevelChanged(playerStats.deffLevel);
            currentMaxHP = playerStats.maxHp;
            currentHP = playerStats.hp;
            currentMaxMana = playerStats.maxMana;
            currentMana = playerStats.mana;
            UpdateHp();
            UpdateMana();
        }

        private void OnPlayersLevelChanged(int level)
        {
            levelText.text = level.ToString();
        }

        private void OnPlayersMaxHpChanged(int maxHp)
        {
            currentMaxHP = maxHp;
            UpdateHp();
        }

        private void UpdateHp()
        {
            hpText.text = currentHP.ToString() + "/" + currentMaxHP.ToString();
        }

        private void OnPlayersHpChanged(int hp)
        {
            currentHP = hp;
            UpdateHp();
        }

        private void OnPlayersMaxManaChanged(int maxMana)
        {
            currentMaxMana = maxMana;
            UpdateMana();
        }

        private void OnPlayersManaChanged(int mana)
        {
            currentMana = mana;
            UpdateMana();
        }

        private void UpdateMana()
        {
            manaText.text = currentMana.ToString() + "/" + currentMaxMana.ToString();
        }
        private void OnPlayersMovementSpeedChanged(int movSpeed)
        {
            movementSpeedText.text = movSpeed.ToString();
        }

        private void OnPlayersJumpForceChanged(int jumpForce)
        {
            jumpForceText.text = jumpForce.ToString();
        }

        private void OnPlayersPowerChanged(int power)
        {
            powerText.text = power.ToString();
        }

        private void OnPlayersDeffLevelChanged(int deffLevel)
        {
            deffendingText.text = deffLevel.ToString();
        }

        private void OnPlayersMagicPowerChanged(int magicPower)
        {
            magicPowerText.text = magicPower.ToString();
        }
    }
}
