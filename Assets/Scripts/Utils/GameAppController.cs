using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameAppController
{
    //Quit the application
    public static void ExitApp()
    {
        Application.Quit();
    }

}
