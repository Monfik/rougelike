using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuQuitButtonScript : MonoBehaviour
{
    public void Exit()
    {
        Application.Quit();
    }
}
