using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    private Animator transition;

    [SerializeField]
    private float transitionTime = 0.8f;

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadSceneWithDelay(sceneIndex));
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneWithDelay(sceneName));
    }

    public void LoadNextScene()
    {
        StartCoroutine(LoadSceneWithDelay(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LoadPreviousScene()
    {
        StartCoroutine(LoadSceneWithDelay(SceneManager.GetActiveScene().buildIndex - 1));
    }

    IEnumerator LoadSceneWithDelay(int sceneIndex)
    {
        transition.SetTrigger("End");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene("Map 1");
    }

    IEnumerator LoadSceneWithDelay(string sceneName)
    {
        transition.SetTrigger("End");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(sceneName);
    }
}
