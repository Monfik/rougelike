using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Tools
{
    public class UtilTools : MonoBehaviour
    {
        // Start is called before the first frame update
        public IEnumerator ExecuteWithDelay(float time, UnityAction func)
        {
            yield return new WaitForSeconds(time);

            func.Invoke();
        }
    }
}
