using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class Values
    {
        public static class InputValues
        {
            //movement input
            public static int D_KEY_MOVE_RIGHT = 101;
            public static int A_KEY_MOVE_LEFT = 102;
            public static int S_KEY_MOVE_DOWN = 103;
            public static int LEFTARROW_KEY_MOVE_LEFT = 110;
            public static int RIGHTARROW_KEY_MOVE_RIGHT = 111;
            public static int SPACE_KEY_JUMP = 120;
            public static int LEFTSHIFT_KEY_RUN = 130;

            //UI
            public static int B_KEY_BACKPACK_OPEN = 200;

            public static float DOUBLE_CLICK_THRESHOLD = 0.25f;

        }
    }
}
