using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace Vocations
{
    public class KnightVocation : Vocation
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            TakeDamage(20);
            
        }
    }

}