using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Effects;
using PlayerControllers;
using BP;
using Data;

namespace Vocations
{
    public abstract class Vocation : MonoBehaviour
    {
        [SerializeField]
        private int level = 1;
        public int Level => level;

        [SerializeField]
        private Stats stats = new Stats();
        public Stats Stats_ => stats;

        [Space(5)]
        [Header("Refs")]
        [SerializeField]
        protected Transform notificationCanvasTransform;

        [SerializeField]
        protected Animator animator;
        public Animator Animatr => animator;

        [SerializeField]
        protected CharacterController2D controller2D;
        public CharacterController2D Controller2D => controller2D;

        [SerializeField]
        protected PlayerMachineState stateMachine;

        public PlayerMachineState StateMachine => stateMachine;

        [SerializeField]
        protected Rigidbody2D rigidBody;
        public Rigidbody2D Rigidbody => rigidBody;

        private DataStorage storage;
        public DataStorage Storage => storage;
        virtual public void Init(DataStorage storage)
        {
            this.storage = storage;

            storage.EventsStorage.PlayerEv.OnItemEquipped += OnItemEquipped;
            storage.EventsStorage.PlayerEv.OnItemDequipped += OnItemDequipped;

            stateMachine.Init(this);
            controller2D.Init(this);
            stateMachine.ChangeState(new PlayerMovementState());
             
        }

        private void Update()
        {
            
        }

        virtual public void OnItemEquipped(EqItem item)
        {
            Stats newStats = stats.Add(item.GetStats());
            Stats.CompareAndCallEvents(newStats, stats, storage.EventsStorage.PlayerEv);
            stats = newStats;
        }
        
        virtual public void OnItemDequipped(EqItem item)
        {
            ScreenConsole.Log("You have dequipped " + item.ItemName);
            Stats newStats = stats.Remove(item.GetStats());
            Stats.CompareAndCallEvents(newStats, stats, storage.EventsStorage.PlayerEv);
            stats = newStats;
        }

        virtual protected void TakeDamage(int damage)
        {
            stats.hp -= damage;
            Mathf.Clamp(stats.hp, 0, stats.maxHp);
            storage.EventsStorage.PlayerEv.CallOnPlayersHpChanged(stats.hp);
            storage.ReferenceStorage.NotificationSpawner.SpawnNotification(damage.ToString(), Color.red, notificationCanvasTransform);

            if(stats.hp <= 0)
            {
                Die();
            }
        }

        virtual protected void Die()
        {
            storage.EventsStorage.PlayerEv.CallOnPlayerDead();
            stateMachine.ChangeState(new PlayerDeadState());
        }

        virtual public void Move(float direction, bool isRunning)
        {
            if(isRunning)
                controller2D.Move(direction * stats.runMovementSpeed);
            else
            {
                controller2D.Move(direction * stats.walkMovementSpeed);
            }
        }

        virtual public void Jump()
        {
            controller2D.Jump(stats.jumpForce);
            stateMachine.ChangeState(new PlayerJumpState());
        }

        virtual public void Land()
        {
            stateMachine.ChangeState(new PlayerMovementState());
        }

        virtual public void Attack()
        {
            stateMachine.ChangeState(new PlayerAttackState());
        }

        virtual public void Idle()
        {
            stateMachine.ChangeState(new PlayerNoneState());
        }

        virtual public void StartMove()
        {
            stateMachine.ChangeState(new PlayerMovementState());
        }
    }

}